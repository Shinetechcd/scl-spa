import {Component} from '@angular/core';

@Component({
	selector:'top-bar',
	templateUrl:'./top.bar.component.html'
})
export class TopBarComponent
{
	title:string='This is top bar';
}
