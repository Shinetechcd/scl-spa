import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SidebarComponent } from './sidebar.component';
import { userResDirective } from '../directives/userResDirective';

@NgModule({
    imports: [ RouterModule, CommonModule ],
    declarations: [ SidebarComponent,userResDirective ],
    exports: [ SidebarComponent ]
})

export class SidebarModule {}
