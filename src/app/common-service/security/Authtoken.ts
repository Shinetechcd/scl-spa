import {Component} from '@angular/core';

export class Authtoken {
  isAuth:boolean;
  accessToken : string;
  expiresIn : number;
  refreshToken : string;
  tokenType : string;
  userFullName : string;
  userId : string;
  userName : string;
}

