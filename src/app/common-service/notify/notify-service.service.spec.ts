import { TestBed, inject } from '@angular/core/testing';

import { NotifyServiceService } from './notify-service.service';

describe('NotifyServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotifyServiceService]
    });
  });

  it('should ...', inject([NotifyServiceService], (service: NotifyServiceService) => {
    expect(service).toBeTruthy();
  }));
});
