import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { APP_BASE_HREF } from '@angular/common';
// import { ROUTER_PROVIDERS } from '@angular/router-deprecated';

import { AppComponent }   from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { DashboardModule } from './dashboard/dashboard.module';
import { SidebarModule } from './sidebar/sidebar.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule} from './shared/navbar/navbar.module';

import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AuthService } from './common-service/security/auth.service';
import { AuthGuard } from './common-service/security/auth-guard.service';
import { GlobeSetsService } from './common-service/globe-data.service';
import { FormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

@NgModule({
  imports: [BrowserModule,
        DashboardModule,
        SidebarModule,
        NavbarModule,
        FooterModule,
        HttpModule,
FormsModule, CustomFormsModule,
        RouterModule.forRoot([])],
    declarations: [AppComponent, DashboardComponent],
    providers: [
        AuthService,
        GlobeSetsService,
        AuthGuard, { provide: LocationStrategy, useClass: HashLocationStrategy }
        ],
    bootstrap: [AppComponent]
})
export class AppModule { }

