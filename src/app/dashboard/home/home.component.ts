import { Component, OnInit } from '@angular/core';
// import initVectorMap = require('../../../assets/js/init/initVectorMap.js');
// import initCharts = require('../../../assets/js/init/charts.js');
// import initAniCharts = require('../../../assets/js/init/initAniCharts.js');
// import initTooltips= require('../../../assets/js/init/initTooltips.js');
//import initNotify = require('../../../assets/js/init/notify.js');



import { NotifyServiceService } from './../../common-service/notify/notify-service.service';
import { NotifyOptions, NotifyAlign, NotifyFrom, NotifyType } from './../../common-service/notify/notify-options';

@Component({
    moduleId: module.id,
    selector: ' home-cmp ',
    templateUrl: 'home.component.html',
    providers:[NotifyServiceService]
})

export class HomeComponent implements OnInit {

    constructor(private ns: NotifyServiceService) { }
    ngOnInit() {
        // initCharts();
        // initVectorMap();
        // initNotify();
        // initAniCharts();
        // initTooltips();
    }

    show = ()=>{
        let notify = new NotifyOptions()
        notify.align = NotifyAlign.Right;
        notify.from = NotifyFrom.Bottom;
        notify.Type = NotifyType.success;
        notify.message = 'this is a test';
        this.ns.labelNotify(notify);
    }
}
