import { NgModule } from '@angular/core';
import { LoginComponent } from './login.component';

import { BrowserModule } from '@angular/platform-browser';

import { LoginRoutingModule } from './login.routing.module';
import { NgxPaginationModule } from 'ngx-pagination'; // <-- import the module
import { Http } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {CustomFormsModule} from 'ng2-validation';
@NgModule({
	imports: [
		BrowserModule,
		LoginRoutingModule,
		NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    CustomFormsModule
	],
	declarations: [
		LoginComponent
	],
	exports: [

	],
	//providers: [OrganizationService],
	entryComponents:[
	]

})
export class LoginModule { }
