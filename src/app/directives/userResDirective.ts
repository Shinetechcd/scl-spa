import {Directive, ElementRef, Renderer, Input, OnInit} from '@angular/core';

@Directive({selector: '[user-Res]'})
export class userResDirective {

  @Input('user-Res')
  userRes : string;
  @Input('res-Action')
  userAction : string;
  constructor(private renderer : Renderer, private el : ElementRef) {}

  ngOnInit() : void {
    var el = this.el.nativeElement;
    var rd = this.renderer;
    var hasPermission = false;
    var userRes = JSON.parse(sessionStorage.getItem('userPermission'));

    if (userRes) {
      for (var i = 0; i < userRes.length; i++) {
        var item = userRes[i];
        if (item.resName === this.userRes && item.action === this.userAction) {
          hasPermission = true;
        }
      }
    }
    if (!hasPermission) {
       el.style.display="none";
      //rd.setElementStyle(el, 'hidden', 'true');
    }
  }
}
