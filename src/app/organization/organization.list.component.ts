import { Component, OnInit, ViewChild } from '@angular/core';
import { OrganizationService } from './services/organization.service';
import { Organization } from './models/organization';
import { Router } from '@angular/router';
import initDataTable = require('../../assets/js/init/initDataTable.js');

@Component({
	selector: 'organization-list',
	templateUrl: './organization.list.component.html',
	styles: [
		`.selected{
			background-color: #cccccc;
		}`
	]
})
export class OrganizationListComponent implements OnInit {
	title: string = 'organizationList';
	orgs: Organization[] = [];
	selectedOrg: Organization;
	loadingState: boolean = false;

	constructor(private service: OrganizationService)
	{
		
	}

	getAll() {
		this.service.getAll().subscribe(
			o => { 
				this.orgs = o; 
				this.orgs = this.orgs.filter(or => !or.isDelete); 
				console.log(this.orgs);				
			}
		);
	}

	ngOnInit() {
		initDataTable();
		this.getAll();
	}

	selectOrg(o: Organization) {
		this.selectedOrg = o;
	}

	update() {
		// if (this.selectedOrg) {
		// 	this.router.navigate(['/organization/edit', this.selectedOrg.id]);
		// }
	}

	add() {
		//this.router.navigate(['/organization/add']);
	}

	delete() {
		// this.modal.confirm()
		// 	.size('lg')
		// 	.isBlocking(true)
		// 	.showClose(true)
		// 	.keyboard(27)
		// 	.title('Your Confirmation')
		// 	.body('Are you sure you wish to do that?')
		// 	.open()
		// 	.then((resultPromise) => {
		// 		console.log(resultPromise.result);
		// 		resultPromise.result.then(
		// 			o => this.doDelete(),
		// 			r => { })
		// 	});
	}

	private doDelete() {
		// this.loadingState = true;
		// this.service.deleteOrganization(this.selectedOrg).subscribe(
		// 	o => {
		// 		this.loadingState = false;
		// 		this.orgs.splice(this.orgs.indexOf(this.selectedOrg), 1);
		// 		this.selectedOrg = null;
		// 	},
		// 	e => {
		// 		this.loadingState = false;
		// 		this.modal.alert()
		// 			.size('lg')
		// 			.showClose(true)
		// 			.title('Error Happens')
		// 			.body(e)
		// 			.open();
		// 	}
		//)
	}
}
