import { SclSpaPage } from './app.po';

describe('scl-spa App', () => {
  let page: SclSpaPage;

  beforeEach(() => {
    page = new SclSpaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
