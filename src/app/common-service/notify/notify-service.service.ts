import { Injectable } from '@angular/core';
import { NotifyOptions, NotifyAlign, NotifyFrom, NotifyType } from './notify-options';

declare var  $:any;
@Injectable()
export class NotifyServiceService {

  constructor() { }

  labelNotify(options: NotifyOptions) {
    $.notify({
      icon: 'notifications',
      message: options.message
    }, {
        type: NotifyType[options.Type].toLowerCase(),
        timer: 3000,
        placement: {
          from: NotifyFrom[options.from].toLowerCase(),
          align: NotifyAlign[options.align].toLowerCase()
        }
      });
  }
}
