import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocationStrategy, PlatformLocation, Location } from '@angular/common';
import { AuthService } from './common-service/security/auth.service';
import { Http } from '@angular/http';
import { AppSettings } from './appsettings';
import { Authtoken } from './common-service/security/Authtoken';
declare var $:any;
@Component({
    selector: 'my-app',
    templateUrl: './app.component.html'
})

export class AppComponent implements OnInit{

    location: Location;
      authService: AuthService;
        http: Http;
         router: Router;
    constructor(location:Location,authService: AuthService,http: Http,router:Router) {
        this.location = location;
         this.authService = authService;
            this.http = http;
              this.router = router;

    }
    ngOnInit(){
        $.getScript('../assets/js/init/initMenu.js');
        $.getScript('../assets/js/demo.js');
     if(!this.authService.loggedIn()){
 this
                  .router
                  .navigate(['/login']);
     }

    }
     // provide local page the user's logged in status (do we have a token or not)
    public isLoggedIn(): boolean {
        return this.authService.loggedIn();
    }

    // tell the server that the user wants to logout; clears token from server, then calls auth.service to clear token locally in browser
    public logout() {
        this.http.get( AppSettings.baseUrl+'/account/logoff', { headers: this.authService.authJsonHeaders() })
            .subscribe(response => {
                // clear token in browser
                this.authService.logout();
                // return to 'home' page
                this.router.navigate(['']);
            },
            error => {
                // failed; TODO: add some nice toast / error handling
                alert(error.text());
                console.log(error.text());
            }
            );
    }
   public isMap(){
        // console.log(this.location);
        if(this.location.prepareExternalUrl(this.location.path()) == '#/maps/fullscreen'){
            return true;
        }
        else {
            return false;
        }
    }
}
