import { Component } from '@angular/core';

@Component
    ({
        selector: 'pageNotFound',
        template: '<h1>Error happens due to there is no such page you want</h1>'
    })
export class PageNotFoundComponent {

}