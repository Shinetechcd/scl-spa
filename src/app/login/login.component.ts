import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';

import {Http} from '@angular/http';
import {AuthService} from '../common-service/security/auth.service';
import {LoginViewModel} from '../models/LoginViewModel';
import {AppSettings} from '../appsettings';

import {FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms'
import {CustomValidators} from 'ng2-validation';

@Component({selector: 'login', templateUrl: './login.html'})

export class LoginComponent implements OnInit {
  formGroup : FormGroup;

  loginViewModel : LoginViewModel;

  constructor(public router : Router, public http : Http, private authService : AuthService) {}

  ngOnInit() {
    this.formGroup = new FormGroup({
      email: new FormControl('', [
        Validators.required
      ]),
       password: new FormControl('', [
        Validators.required,
        Validators.minLength(3)
      ])
    });
    this.loginViewModel = new LoginViewModel();

  }
  onSubmit() : void {


    if (this.formGroup.valid) {
      this
        .http
        .post(AppSettings.baseApiUrl + 'token/getToken', this.loginViewModel, {
          headers: this
            .authService
            .jsonHeaders()
        })
        .subscribe(tokenResponse => {
          var tokenResult = tokenResponse.json();
          if (tokenResult.status != 0) {
            // success, save the token to session storage
            this
              .authService
              .login(tokenResult);

            //get user permission
            this
              .http
              .get(AppSettings.baseApiUrl + 'User/GetUserRes', {
                headers: this
                  .authService
                  .authJsonHeaders()
              })
              .subscribe(permissionResponse => {
                var permissionResult = permissionResponse.json();
                if (permissionResult.status != 0) {
                  sessionStorage.setItem('userPermission', JSON.stringify(permissionResult.result));
                  this
                    .router
                    .navigate(['/dashboard']);
                } else {
                  alert(permissionResult.message);
                  console.log(permissionResult.message);
                }
              }, error => {
                // failed; TODO: add some nice toast / error handling
                alert(error.text());
                console.log(error.text());
              });

          } else {
            alert(tokenResult.message);
            console.log(tokenResult.message);
          }

        }, error => {
          // failed; TODO: add some nice toast / error handling
          alert(error.text());
          console.log(error.text());
        });
    }
  }

}
