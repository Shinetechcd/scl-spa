import { Component, OnInit, Input } from '@angular/core';
import { OrganizationService } from './services/organization.service';
import { Organization } from './models/organization';
import { Region } from './models/region';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import { Observable } from "rxjs/Observable";

@Component
	({
		selector: 'organization-edit',
		templateUrl: './organization.edit.component.html',
		styles: [

		]
	})
export class OrganizationEditComponent implements OnInit {
	private org: Organization;
	@Input("parent") parent: any;
	regions: Region[] = [];
	organizations: Organization[] = [];
	isError: boolean = false;
	error: string;
	loadingState: boolean = false;

	ngOnInit() {
		this.activatedRoute.params.switchMap((params: Params) => {
			if (params['id']) {
				return this.service.getById(params['id']);
			}
			else {

				return Observable.of(new Organization());
			}
		}).subscribe((o: Organization) => {
			this.org = o;
			console.log(this.org);
			this.service.getAll().subscribe(
				o => {
					this.organizations = o;
					this.organizations = this.organizations.filter(oo => this.org.id !== oo.id && !oo.isDelete);
				}
			);
		});

		this.service.getAllRegion().subscribe(
			o => this.regions = o
		);
	}
	constructor(private service: OrganizationService, private router: Router, private activatedRoute: ActivatedRoute) {

	}

	updateOrg() {
		this.loadingState = true;
		this.isError = false;
		console.log(this.org);
		if (this.org.id === '') {
			this.service.addOrganization(this.org).subscribe(
				o => this.router.navigate(['/organization']),
				e => {
					this.loadingState = false;
					this.isError = true;
					this.error = e;
					console.log(e);
				});
		}
		else {
			this.service.updateOrganization(this.org).subscribe(
				o => this.router.navigate(['/organization']),
				e => {
					this.loadingState = false;
					this.isError = true;
					this.error = e;
					console.log(e);
				});
		}
	}

	back() {
		this.router.navigate(['/organization']);
	}
}
