import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrganizationListComponent } from './organization.list.component';
import { OrganizationEditComponent } from './organization.edit.component';

const organizationRoutes: Routes = [
    {
        path: 'organization',
        component: OrganizationListComponent
    },
    {
        path: 'organization/edit/:id',
        component: OrganizationEditComponent
    },
    {
        path: 'organization/add',
        component: OrganizationEditComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(organizationRoutes)],
    exports: [RouterModule]
})
export class OrganizationRoutingModule { }
