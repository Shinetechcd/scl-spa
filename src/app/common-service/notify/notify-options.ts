export class NotifyOptions {
    from: NotifyFrom;
    align: NotifyAlign;
    Type: NotifyType;
    message: string;
}
export enum NotifyFrom {
    Top,
    Bottom
}
export enum NotifyAlign {
    Left,
    Center,
    Right
}
export enum NotifyType {
    info,
    success,
    warning,
    danger,
    rose,
    primary
}