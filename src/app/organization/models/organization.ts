export class Organization {
	public id: string = '';
	public name: string = '';
	public regionName: string = '';
	public regionId: string = '';
	public superior?: string = null;
	public superiorId?: string = null;
	public isDelete: boolean = false;
	constructor() {

	}
}
