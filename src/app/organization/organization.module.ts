import { NgModule } from '@angular/core';
import { OrganizationListComponent } from './organization.list.component';
import { OrganizationEditComponent } from './organization.edit.component';
import { OrganizationService } from './services/organization.service';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { OrganizationRoutingModule } from './organization.routing.module';
import { NgxPaginationModule } from 'ngx-pagination'; // <-- import the module
import { Http } from '@angular/http';

@NgModule({
	imports: [
		BrowserModule,
		FormsModule,
		OrganizationRoutingModule,
		NgxPaginationModule
	],
	declarations: [
		OrganizationListComponent,
		OrganizationEditComponent
	],
	exports: [

	],
	providers: [OrganizationService],
	entryComponents:[
	]

})
export class OrganizationModule { }
