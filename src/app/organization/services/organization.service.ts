import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Organization } from '../models/organization';
import { Observable } from "rxjs/Observable";
import { AppSettings } from '../../appsettings';

@Injectable()
export class OrganizationService {
	constructor(private http: Http) { }
	private baseUrl = AppSettings.baseUrl + 'organizations';

	getAll() {
		var url = this.baseUrl + '/all';
		return this.http.get(url).map(response => response.json().message.result);
	}

	getById(id: string) {
		var url = this.baseUrl + '/' + id;
		return this.http.get(url).map(response => response.json().result);
	}

	getAllRegion() {
		var url = this.baseUrl + '/regions/all';
		return this.http.get(url).map(response => response.json().result);
	}

	updateOrganization(org: Organization) {
		var url = this.baseUrl + '/update';
		var data = {
			Id: org.id,
			Name: org.name,
			SuperiorId: org.superiorId,
			RegionId: org.regionId
		};
		return this.http.put(url, data, new RequestOptions({
			headers: new Headers({
				"Content-Type": "application/json"
			})
		})).map(response => { console.log(response); return response.json(); }).catch(this.handleError);
	}

	addOrganization(org: Organization) {
		var url = this.baseUrl + '/add';
		var data = {
			Name: org.name,
			SuperiorId: org.superiorId,
			RegionId: org.regionId
		};
		return this.http.post(url, data, new RequestOptions({
			headers: new Headers({
				"Content-Type": "application/json"
			})
		})).map(response => {
			if (response.status == 201) {
				return [{ status: response.status }];
			}
		}).catch(this.handleError);
	}

	deleteOrganization(org: Organization) {
		var url = this.baseUrl + '/delete/' + org.id;

		return this.http.delete(url).map(response => {
			console.log(response);
			if (response.status == 204) {
				return [{ status: response.status }];
			}
		}).catch(this.handleError);
	}

	private handleError(error: Response | any) {
		console.log(error);
		// In a real world app, you might use a remote logging infrastructure
		let errMsg: string;
		let body;
		if (error instanceof Response) {
			body = error.json() || '';
			const err = body.error || JSON.stringify(body);
			errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
		} else {
			errMsg = error.message ? error.message : error.toString();
		}
		console.log(error);
		console.error(errMsg);
		console.log(body);
		return Observable.throw(body.message);
	}
}
