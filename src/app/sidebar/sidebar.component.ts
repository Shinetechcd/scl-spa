import {Component, OnInit} from '@angular/core';
import {ROUTES} from './sidebar-routes.config';
import {MenuType} from './sidebar.metadata';
import {AuthService} from '../common-service/security/auth.service';


declare var $ : any;

@Component({moduleId: module.id, selector: 'sidebar-cmp', templateUrl: 'sidebar.component.html'
})

export class SidebarComponent implements OnInit {
  public menuItems : any[];
  public isLogin : boolean;
  constructor(private authService : AuthService) {}
  ngOnInit() {
    $.getScript('../../assets/js/sidebar-moving-tab.js');
    this.isLogin = this
      .authService
      .loggedIn();
    this.menuItems = ROUTES.filter(menuItem => menuItem.menuType !== MenuType.BRAND);
  }

}
